FROM golang:latest 
RUN mkdir /app 
ADD . /app/ 
WORKDIR /app 
RUN CGO_ENABLED=0 GOOS=linux go build -a -ldflags '-extldflags "-static"' -o main . 

FROM scratch
WORKDIR /app 
COPY --from=0 app/main .
CMD ["/app/main"]
